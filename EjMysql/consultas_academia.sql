use academiax2_tarde;
--      INSERT EN TABLAS PROFESORES TUTOR CURSOS Y EDICIONES
-- insert into profesores (nombre) values ("ricard"), ("adria"), ("eric");
-- insert into tutor (nombre) values ("sole"), ("eric");
-- insert into cursos (descripcion, lenguaje, horas) values ("Avanzado programacion web con java","java",275),
--     ("Programacion avanzada .Net",".Net",250),("Programacion Python","python",150);
-- insert into ediciones (cursos_idcursos, matitarda, finicio, profesores_idprofesores, tutor_idtutor)
--     values (3, 2,"2019-02-18", 3, 4);

--      INSERTS EN TABLA ALUMNOS
-- insert into alumnos (nombre,email) values ("edgar","edgar@gmail.com"),("juanka","juanka@gmail.com"),("marc","marc@gmail.com"),
--     ("isma","isma@gmail.com"),("juan","juan@gmail.com"),("toni","toni@gmail.com"),("mangel","mangel@gmail.com"),
--     ("marti","marti@gmail.com"),("sebas","sebas@gmail.com"),("melanie","melanie@gmail.com"),("juanka2","juanka2@gmail.com"),
--     ("martin","martin@gmail.com"),("david","david@gmail.com"),("bryan","bryan@gmail.com"),("jordi","jordi@gmail.com");
-- 
--      INSERT EN TABLA ORDENADORES
-- insert into ordenadores (modelo,marca,anyo_compra,numero) values ("ordenador1","acer","2005",12),("ordenador2","acer","2005",13),
--     ("ordenador3","acer","2009",14),("ordenador6","toshiba","2009",17),("ordenador9","toshiba","2009",20),("ordenador12","toshiba","2008",23),
--     ("ordenador4","lenovo","2008",15),("ordenador7","lenovo","2010",18),("ordenador10","lenovo","2010",21),("ordenador13","mac","2010",24),
--     ("ordenador5","mac","2017",16),("ordenador8","mac","2017",19),("ordenador11","HP","2017",22),("ordenador14","HP","2004",25),
--     ("ordenador15","HP","2004",26);

--      INSERT EN TABLA MATRICULAS
-- insert into matricula (ediciones_idediciones,alumnos_idalumnos,ordenadores_idordenadores) values
-- 	(4,20,41),(4,21,42),(4,22,43),(4,23,44),(4,24,45),(4,25,46),(4,26,47),(4,27,48),(6,28,49),(6,29,50),
--     (6,30,51),(6,31,52),(6,32,53),(6,33,54),(6,34,55);
--
--      PRUEBA ENTRE TABLAS SIN JOINS
-- select a.nombre,c.descripcion from matricula m,alumnos a,cursos c, ediciones e
--     where a.idalumnos=m.alumnos_idalumnos and m.ediciones_idediciones=e.idediciones and 
--     e.cursos_idcursos=c.idcursos;
--      
--      MODELO , MARCA DE ORDENADOR, NUMERO DE ORDENADOR Y NOMBRE DE ALUMNO
-- SELECT o.marca,o.modelo,o.numero,a.nombre,c.descripcion FROM alumnos a
--     join matricula m on a.idalumnos=m.alumnos_idalumnos
--     join ediciones e on m.ediciones_idediciones=e.idediciones
--     join cursos c on e.cursos_idcursos=c.idcursos
--     join ordenadores o on m.ordenadores_idordenadores=o.idordenadores;

--      NOMBRE ALUMNO Y NOMBRE PROFESOR
-- SELECT a.nombre as "Nombre alumno",p.nombre as "Nombre profesor" FROM ediciones e 
--     join profesores p on e.profesores_idprofesores=p.idprofesores
--     join matricula m on m.ediciones_idediciones=e.idediciones
--     join alumnos a on a.idalumnos=m.alumnos_idalumnos;

--      LENGUAJE DEL CURSO, NOMBRE ALUMNO, MARCA ORDENADOR
-- select a.nombre,c.descripcion, o.marca from matricula m
--     join alumnos a on a.idalumnos=m.alumnos_idalumnos
--     join ediciones e on m.ediciones_idediciones=e.idediciones
--     join cursos c on e.cursos_idcursos=c.idcursos
--     join ordenadores o on o.idordenadores=m.ordenadores_idordenadores;

--      DESCRIPCION DEL CURSO,FECHA DE INICIO Y NUMERO DE ALUMNOS MATRICULADOS
-- select c.descripcion, e.finicio,count(a.nombre) as "Numero de alumnos" from matricula m
--     join alumnos a on a.idalumnos=m.alumnos_idalumnos
--     join ediciones e on m.ediciones_idediciones=e.idediciones
--     join cursos c on e.cursos_idcursos=c.idcursos
--     group by c.idcursos;
--      EJERCICIOS UPDATES
-- create table generos select distinct(genre) from ventas;
-- update ventas v left join generos g on v.genre=g.genre set v.id_genero=g.id_genero;
-- create table plataformas select distinct(platform) from ventas;
-- update ventas v left join plataformas p on v.platform=p.plataforma set v.id_plataforma=p.id_plataforma;
-- create table editores select distinct(publisher) from ventas;
-- update ventas v left join editores e on v.publisher=e.publisher set v.id_editor=e.id_editores;

    