function mayor(a, b) {
    if (a > b) {
        return a;
    }
    return b;
}
function datos(x) {
    var a;
    a = x % 2;
    if (a === 0) {
        console.log(x + " es un numero par.");
    }
    else {
        console.log(x + " es un numero impar.");
    }
    a = x % 3;
    if (a === 0) {
        console.log(x + " SI es divisible por 3.");
    }
    else {
        console.log(x + " NO es divisible por 3.");
    }
    a = x % 5;
    if (a === 0) {
        console.log(x + " SI es divisible por 5.");
    }
    else {
        console.log(x + " NO es divisible por 5");
    }
    a = x % 7;
    if (a === 0) {
        console.log(x + " SI es divisible por 7.");
    }
    else {
        console.log(x + " NO es divisible por 7");
    }
    return "Fin del programa";

}
function factorial(x) {
    var total = 1;
    for (i = 1; i <= x; i++) {
        total = total * i;
    }
    return total;
}
function capitaliza(x) {
    var primeraLetra;
    var resto;
    primeraLetra = x.substring(0, 1);
    resto = x.substring(1);
    primeraLetra = primeraLetra.toUpperCase();
    resto = resto.toLowerCase();
    return primeraLetra + resto;
}
function palabra(x) {
    var cont;
    var a;
    cont = x.length;
    console.log(x + " tiene " + cont + " letras.");
    a = cont % 2;
    if (cont === 0) {
        console.log(cont + " es un numero par.");
    }
    else {
        console.log(cont + " es un numero impar.");
    }
    var contVocales = 0;
    for (a = 0; a !== cont; a++) {
        if (x.charAt(a) == "a" || x.charAt(a) == "e" || x.charAt(a) == "i" || x.charAt(a) == "o" || x.charAt(a) == "u") {
            contVocales++;
        }
    }
    console.log("La palabra tiene " + contVocales + " vocales.");
    cont = cont - contVocales;
    return ("La palabra tiene " + cont + " consonantes.");
}
function hoy() {
    var dias = ["Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"];
    var fecha = new Date();
    var dia = fecha.getDay();
    return dias[dia - 1];
}
function analiza(numeros) {
    var suma = 0;
    var numMayor = numeros[0];
    var numMenor = numeros[0];
    var cont = 0;
    for (cont = 0; cont != numeros.length; cont++) {
        suma = suma + numeros[cont];
        if (numeros[cont] > numMayor) {
            numMayor = numeros[cont];
        }
        if (numMenor > numeros[cont]) {
            numMenor = numeros[cont];
        }
    }
    console.log("La suma de los " + numeros.length + " numeros es " + suma);
    console.log("El mayor numero es " + numMayor);
    return ("El menor numero es " + numMenor);
}
function navidad() {
    var fechaNavidad = new Date(2019,11,26);
    var milisegundosNavidad = fechaNavidad.getTime();
    var fechaActual = new Date();
    var milisegundosActual = fechaActual.getTime();
    var milisegundos = milisegundosNavidad - milisegundosActual;
    var diasNavidad=0;
    diasNavidad = milisegundos / 1000;
    diasNavidad = diasNavidad /3600;
    diasNavidad = diasNavidad / 24;
    diasNavidad = Math.round(diasNavidad);

    return ("Quedan "+diasNavidad+" dias para Navidad");
}
var numeros = [2, 4, 8, -2];
console.log("____________________________");
console.log("|Ejercicio mayor o menor:");
console.log("|"+mayor(2, 4));
console.log("|Ejercicio datos numeros:");
console.log("|"+datos(1240));
console.log("|__________________________");
console.log("|Ejercicio factorial:");
console.log("|"+factorial(10));
console.log("|__________________________");
console.log("|Capitaliza:");
console.log("|"+capitaliza("barcelona"));
console.log("|"+capitaliza("PARIS"));
console.log("|___________________________");
console.log("|Palabra:");
console.log("|"+palabra("Barcelona"));
console.log("|___________________________");
console.log("|Hoy:");
console.log("|"+hoy());
console.log("|__________________________");
console.log("|Navidad:");
console.log("|"+navidad());
console.log("|__________________________");
console.log("|Analiza:");
console.log("|"+analiza(numeros));
console.log("|__________________________");
