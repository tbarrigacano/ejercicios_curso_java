
public class Persona {
    String nombre;
    int edad;

    void saluda() {
        System.out.println("Hola me llamo "+this.nombre);
    }
    void saluda(Persona otra){
        int diferenciEdad;
        if (this.edad>otra.edad){
            diferenciEdad=this.edad-otra.edad;
            System.out.println("Hola me llamo "+this.nombre+" y soy mayor que "+otra.nombre+" y tengo "+diferenciEdad+" mas");
            
        }
        else {
            diferenciEdad=otra.edad-this.edad;
            System.out.println("Hola me llamo "+this.nombre+" y soy mas joven que "+otra.nombre+" y tengo "+diferenciEdad+" menos");
            
        }


    }

}
