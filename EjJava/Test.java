
public class Test {
    public static void main(String[] args) {
        
        Persona marta = new Persona();
        marta.nombre = "Marta";
        marta.edad = 20;

        Persona isabel = new Persona();
        isabel.nombre = "Isabel";
        isabel.edad = 22;
        
        marta.saluda(isabel);
        isabel.saluda(marta);
        

    }
}